in vec3 new_colour;

out vec4 out_Color;

void main(void){
	out_Color = vec4(new_colour,1.0);
}